package fbmoduleone

import "testing"

func TestFbModuleOneAbout(t *testing.T) {
	expected := "I am from foobar multi module repo fbmodule one example."
	actual := About()
	if actual != expected {
		t.Errorf("Expected: %s Actual: %s", expected, actual)
	}
}
