package exampletwo

import "testing"

func TestFbModuleThreeExampleTwoAbout(t *testing.T) {
	expected := "I am from foobar multi module repo fbmodulethree example 2."
	actual := About()
	if actual != expected {
		t.Errorf("Expected: %s Actual: %s", expected, actual)
	}
}
