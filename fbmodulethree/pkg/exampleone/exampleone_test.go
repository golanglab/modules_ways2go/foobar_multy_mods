package exampleone

import "testing"

func TestFbModuleThreeExampleOneAbout(t *testing.T) {
	expected := "I am from foobar multi module repo fbmodulethree example 1."
	actual := About()
	if actual != expected {
		t.Errorf("Expected: %s Actual: %s", expected, actual)
	}
}
