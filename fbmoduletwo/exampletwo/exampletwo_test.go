package exampletwo

import "testing"

func TestFbModuleTwoExampleTwoAbout(t *testing.T) {
	expected := "I am from foobar multi module repo fbmoduletwo example 2."
	actual := About()
	if actual != expected {
		t.Errorf("Expected: %s Actual: %s", expected, actual)
	}
}
